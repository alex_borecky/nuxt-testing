import Vue from 'vue'

import secondaryButton from '@/components/secondaryButton.vue'
import projectPreview from '@/components/projectPreview.vue'
import projectButton from '@/components/projectButton.vue'
import projectHeader from '@/components/projectHeader.vue'
import projectStatus from '@/components/projectStatus.vue'
import projectDescription from '@/components/projectDescription.vue'
import projectImage from '@/components/projectImage.vue'
import imageText from '@/components/imageText.vue'
import externalPreview from '@/components/externalPreview.vue'
import externalButton from '@/components/externalButton.vue'
import VueEmbedGist from 'vue-embed-gist'

Vue.component('projectPreview', projectPreview)
Vue.component('secondaryButton', secondaryButton)
Vue.component('projectButton', projectButton)
Vue.component('projectHeader', projectHeader)
Vue.component('projectStatus', projectStatus)
Vue.component('projectDescription', projectDescription)
Vue.component('projectImage', projectImage)
Vue.component('imageText', imageText)
Vue.component('externalPreview', externalPreview)
Vue.component('externalButton', externalButton)
Vue.component('vue-embed-gist', VueEmbedGist)
